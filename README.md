# phalcon-docker

## how to use
* for windows users, clone this repo into C://Users/${User} folder
* copy your project into src folder
* update .env file. Windows users should pass absolute path in format what presented in default .env. Linux users may pass relative path
* run docker-compose up --build. --build flag use only for the first setup. Next time use docker-compose up
